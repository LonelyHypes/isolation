using UnityEngine;
using System.Collections;

// Class used to slow down game speed 

public class SlowDownTime : MonoBehaviour {

	// If Q button is pressed set time scale to 0.65 
	// Else game speed remains default
	void Update () {
        if (Input.GetKey(KeyCode.Q))
        {
            Time.timeScale = 0.65f;
        }
        else
            Time.timeScale = 1;
    }	
}
