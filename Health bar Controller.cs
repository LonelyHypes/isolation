using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Class used for player health bar
public class HealthbarController : MonoBehaviour {


		public Image healthBar;
		public float health;
		public float startHealth;
		

	// Setting method that will be called if player is damaged 
	public void onTakeDamage(int damage) 

	// Subtract damage from current health which is = to new health 
	// Sets fill amount of health bar to a lower point
	{
		health = health - damage;
		healthBar.fillAmount = health / startHealth;
	}


	// Resets level if health  is <=0
	// will send player back to starting position not checkpoint

	void Update () 
	{

		if(health <= 0)
		{
			health = 0;  Application.LoadLevel (Application.loadedLevel);
		}
	}

}

