using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


// Class to respawn player using various methods
	public class Respawn : MonoBehaviour {

	// Set levelN to = 0
	public static int levelN = 0;

	// Determine start position
	private Vector3 startPos;
	private Quaternion startRot;


	// Setting start position
	void Start () 
	{
		startPos = transform.position;
		startRot = transform.rotation;
	}


	// set next level function 
	void nextLevel()
	{
		levelN++;

		if(levelN > 1)
		{
			levelN = 0;
		}
		SceneManager.LoadScene(levelN);
	}

	// Dectects collison using enter trigger
	void OnTriggerEnter(Collider col)

	// If tag is  = death set player to start position & rotation
	// If tag is  = checkpoint set new start position and rotation to collider object, destroy collider object
	// If tag is  = goal destroy goal object, load next level.

	{
		if(col.tag == "death")
		{
			transform.position = startPos;
			transform.rotation = startRot;
			GetComponent<Animator>().Play("LOSE00",-1,0f);
			GetComponent<Rigidbody>().velocity = new Vector3(0f,0f,0f);
			GetComponent<Rigidbody>().angularVelocity = new Vector3(0f,0f,0f);
		}
		else if(col.tag == "checkpoint")
		{
			startPos = col.transform.position;
			startRot = col.transform.rotation;
			Destroy(col.gameObject);
			GetComponent<Animator>().Play("WIN00",-1,0f);
		}
		else if(col.tag == "goal")
		{
			Destroy(col.gameObject);
			SceneManager.LoadScene(levelN);
		}
	}
}
