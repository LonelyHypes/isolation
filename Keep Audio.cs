using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Keeps audio playing between scenes
public class KeepAudio : MonoBehaviour
{

	// Doesn't destroy audio between scene changes
	void awake()
	{
		DontDestroyOnLoad (transform.gameObject);
	}

}