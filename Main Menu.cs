using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Class for start screen / main menu 
public class MainMenu : MonoBehaviour {

	// On Play Game load next scene
	public void PlayGame () 
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

	// On Quit Game Application Quit
	// Display Quit in debug log
	public void QuitGame ()
	{
		Debug.Log ("QUIT!");
		Application.Quit();
	}

}	