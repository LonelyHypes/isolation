using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionController : MonoBehaviour
{
	// Calling Health Bar Controller
	// Making Instance of HealthBarController
	public HealthbarController healthbar;

	// If object enter collision check object game tag
	// If health bar take damage
	void  OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "EvilCube") {
			if (healthbar) {
				healthbar.onTakeDamage (10);
			}
		}
	}
}